# vim:fileencoding=utf-8:noet
from __future__ import (unicode_literals, division, absolute_import, print_function)

from datetime import datetime
import pytz

def time(pl, format='%H:%M %Z', timezone=pytz.utc.zone):
    '''Return the current time in a given timezone.

    :param str format:
            strftime-style date format string
    :param str timezone:
            pytz timezone name

    Divider highlight group used: ``time:divider``.

    Highlight groups used: ``time`` and ``date``.
    '''

    tz = pytz.timezone(timezone)
    utc_dt = datetime.now(pytz.utc)
    loc_dt = utc_dt.astimezone(tz)
    try:
        contents = loc_dt.strftime(format)
    except UnicodeEncodeError:
        contents = loc_dt.strftime(format.encode('utf-8')).decode('utf-8')

    return [{
            'contents': contents,
            'highlight_groups': ['time', 'date'],
            'divider_highlight_group': 'time:divider'
    }]
