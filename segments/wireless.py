# vim:fileencoding=utf-8:noet
from __future__ import (unicode_literals, division, absolute_import, print_function)

import os
from powerline.theme import requires_segment_info

@requires_segment_info
def wireless(pl, segment_info, device=None, format='{quality:3.0%} at {essid}',
    short_format='{quality:3.0%}', format_down=None, auto_shrink=False):
    '''Returns the current connection quality.
    :param string device:
        the device to use. Per default this segment will try to be smart.
    :param string format:
        the output format
    :param string short_format:
        optional shorter format when the powerline needs to shrink segments
    :param string format_down:
        if set to any other value than ``None``, it will be shown when no wireless connection is
        present.
    :param bool auto_shrink:
        if set to true, this segment will use ``short_format`` per default,
        only using ``format`` when any message is present on the ``net.wireless``
        message channel.
    Highlight groups used: ``wireless:quality`` (gradient), ``wireless:down`` alternatively ``wireless:quality`` (gradient)
    Click values supplied: ``quality`` (int), ``essid`` (string)
    '''
    payload_name = 'net.wireless'

    if not device:
        for interface in os.listdir('/sys/class/net'):
            if interface.startswith('wl'):
                device = interface
                break

    try:
        import iwlib
    except ImportError:
        pl.info("Couldn't load iwlib")
        return None if not format_down else [{
            'contents': format_down.format(quality=0, essid=None, frequency=0),
            'highlight_groups': ['wireless:down', 'wireless:quality', 'quality_gradient'],
            'gradient_level': 100,
            'payload_name': payload_name
        }]

    stats = iwlib.get_iwconfig(device)
    stats = {a.lower(): stats[a].decode() if isinstance(stats[a], bytes) else stats[a] for a in stats}

    quality = 0
    essid = ''
    if 'essid' in stats:
        essid = stats['essid']
        if 'stats' in stats and 'quality' in stats['stats']:
            quality = stats['stats']['quality']

    if essid == '' or quality == 0:
        return None if not format_down else [{
            'contents': format_down.format(quality=0, essid=None, **stats),
            'highlight_groups': ['wireless:down', 'wireless:quality', 'quality_gradient'],
            'gradient_level': 100,
            'payload_name': payload_name
        }]

    if not auto_shrink or ('payloads' in segment_info and payload_name in
        segment_info['payloads'] and segment_info['payloads'][payload_name]):
            return [{
                'contents': format.format(quality=quality/70, **stats),
                'highlight_groups': ['wireless:quality', 'quality_gradient'],
                'gradient_level': 100 * (70 - quality) / 70,
                'click_values': {'essid': essid, 'quality': quality * 100 / 70},
                'payload_name': payload_name
                }]
    return [{
        'contents': short_format.format(quality=quality/70, **stats),
        'highlight_groups': ['wireless:quality', 'quality_gradient'],
        'gradient_level': 100 * (70 - quality) / 70,
        'click_values': {'essid': essid, 'quality': quality * 100 / 70},
        'payload_name': payload_name,
        'truncate': lambda a,b,c: short_format.format(quality=quality/70, **stats)
        }]
